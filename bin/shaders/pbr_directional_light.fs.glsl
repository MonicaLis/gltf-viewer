#version 330
in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform float uOcclusionStrength;

uniform int uApplyOcclusion;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;
uniform vec3 uEmissiveFactor;

uniform vec4 uBaseColorFactor;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessFromTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOcclusionTexture;


out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{
  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDirection;
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);


  vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
  vec4 metallicRoughnessFromTexture =
      SRGBtoLINEAR(texture(uMetallicRoughnessFromTexture, vTexCoords));
  vec3 emissive =
      SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

  vec4 baseColor = baseColorFromTexture * uBaseColorFactor;
  vec3 metallic = vec3(uMetallicFactor * metallicRoughnessFromTexture.b);
  float roughness = uRoughnessFactor * metallicRoughnessFromTexture.g;


  // ----------------------------------------------------------------
  // From KhronosGroup:
  // c_diff = lerp(baseColor.rgb * (1 - dielectricSpecular), black, metallic)
  // f0 = lerp(0.04, baseColor.rgb, metallic)
  // α = roughness^2

  // F = f0 + (1 - f0) * (1 - abs(VdotH))^5

  // f_diffuse = (1 - F) * (1 / π) * c_diff
  // f_specular = F * D(α) * G(α) / (4 * abs(VdotN) * abs(LdotN))

  // material = f_diffuse + f_specular
  // ----------------------------------------------------------------

  vec3 black = vec3(0.);
  vec3 dieletricSpecular = vec3(0.04);

  vec3 c_diff = mix(baseColor.rgb * (1 - dieletricSpecular.r), black, metallic);
  vec3 f0 = mix(vec3(0.04), baseColor.rgb, metallic);

  float VdotH = clamp(dot(V,H), 0., 1.);
  float alpha = roughness * roughness;

  // Compute baseShlickFactor first
  float baseShlickFactor = 1 - VdotH;
  float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
  shlickFactor *= shlickFactor; // power 4
  shlickFactor *= baseShlickFactor; // power 5

  vec3 F = f0 + (1 - f0) * shlickFactor;

  float sqrAlpha = alpha * alpha;

  float NdotL = clamp(dot(N, L), 0., 1.);
  float NdotV = clamp(dot(N, V), 0., 1.);

  float visDenominator =
  NdotL * sqrt(NdotV * NdotV * (1 - sqrAlpha) + sqrAlpha) +
  NdotV * sqrt(NdotL * NdotL * (1 - sqrAlpha) + sqrAlpha);
  float Vis = visDenominator > 0. ? 0.5 / visDenominator : 0.0;

  float NdotH = clamp(dot(N, H), 0., 1.);
  float baseDenomD = (NdotH * NdotH * (sqrAlpha - 1.) + 1.);
  float D = M_1_PI * sqrAlpha / (baseDenomD * baseDenomD);

  vec3 f_specular = F * Vis * D;

  vec3 diffuse = c_diff * M_1_PI;

  vec3 f_diffuse = (1. - F) * diffuse;

  vec3 color = (f_diffuse + f_specular) * uLightIntensity * NdotL;
  color = color + emissive;

  // if 1 ie if true
  if (uApplyOcclusion == 1)
  {
    float applyOcc = texture2D(uOcclusionTexture, vTexCoords).r;
    color = mix(color, color * applyOcc, uOcclusionStrength);
  }

  fColor = LINEARtoSRGB(color);

}
