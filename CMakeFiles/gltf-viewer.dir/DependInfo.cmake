# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/monica/Documents/S6/gltf-viewer/third-party/glad/src/glad.c" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/glad/src/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLM_ENABLE_EXPERIMENTAL"
  "IMGUI_IMPL_OPENGL_LOADER_GLAD"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "third-party/glfw-3.3.1/include"
  "third-party/glm-0.9.9.7"
  "third-party/glad/include"
  "third-party/imgui-1.74"
  "third-party/imgui-1.74/examples"
  "third-party/tinygltf-bcf2ce586ee8bf2a2a816afa6bfe2f8692ba6ac2/include"
  "third-party/args-6.2.2"
  "lib/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/monica/Documents/S6/gltf-viewer/src/ViewerApplication.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/ViewerApplication.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/main.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/main.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/tiny_gltf_impl.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/tiny_gltf_impl.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/utils/cameras.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/cameras.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/utils/gl_debug_output.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/gl_debug_output.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/utils/gltf.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/gltf.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/src/utils/images.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/src/utils/images.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/examples/imgui_impl_glfw.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_glfw.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/examples/imgui_impl_opengl3.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/examples/imgui_impl_opengl3.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/imgui.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/imgui_demo.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_demo.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/imgui_draw.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_draw.cpp.o"
  "/home/monica/Documents/S6/gltf-viewer/third-party/imgui-1.74/imgui_widgets.cpp" "/home/monica/Documents/S6/gltf-viewer/CMakeFiles/gltf-viewer.dir/third-party/imgui-1.74/imgui_widgets.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLM_ENABLE_EXPERIMENTAL"
  "IMGUI_IMPL_OPENGL_LOADER_GLAD"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "third-party/glfw-3.3.1/include"
  "third-party/glm-0.9.9.7"
  "third-party/glad/include"
  "third-party/imgui-1.74"
  "third-party/imgui-1.74/examples"
  "third-party/tinygltf-bcf2ce586ee8bf2a2a816afa6bfe2f8692ba6ac2/include"
  "third-party/args-6.2.2"
  "lib/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/monica/Documents/S6/gltf-viewer/third-party/glfw-3.3.1/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
