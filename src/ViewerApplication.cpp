#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

// render to image

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto lightDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto lightIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  const auto baseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto baseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto roughnessFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto metallicRoughnessFromTextLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessFromTexture");
  const auto metallicFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto emissiveFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto emissiveTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");

  const auto occlusionTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto occlusionStrengthLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto applyOcclusionLocation =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltfFile(model)) {
    return -1;
  }

  // Bounding box
  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  // Light variables
  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightFromCamera = false;
  bool applyOcclusion = true;

  // Build projection matrix
  const auto diagonal = bboxMax - bboxMin;
  auto maxDistance = glm::length(diagonal);
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // CameraController model implementation
  std::unique_ptr<CameraController> cameraController =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), 0.25f * maxDistance);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera); // -> because it's a pointer!
  } else {
    // TODO Use scene bounds to compute a better default camera
    const auto bboxCentre = (bboxMin + bboxMax) / 2.f;
    const auto upVector = glm::vec3(0, 1, 0);
    // if the scene is flat (if the diagonal is parallel to the z axis)
    const auto eye = diagonal.z == 0.f
                         ? bboxCentre + 2.f * glm::cross(diagonal, upVector)
                         : bboxCentre + diagonal;
    cameraController->setCamera(Camera{eye, bboxCentre, upVector});
  }

  // Create texture objects
  const std::vector<GLuint> textureObjects = createTextureObjects(model);

  GLuint whiteTexture = 0;
  // Create a white texture for objects with no base color texture
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  const std::vector<GLuint> bufferObjects = createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshToVertexArrays;
  const std::vector<GLuint> vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshToVertexArrays);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // Material binding
  const auto bindMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      // If there's a base color factor
      if (baseColorFactorLocation >= 0) {
        glUniform4f(baseColorFactorLocation,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      // If the material has a base color texture
      if (baseColorTextureLocation >= 0) {
        // Give it the default white texture just in case
        auto textureObject = whiteTexture;
        // Always check indexes >= 0 !!
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];

          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        // Bind textureObject to target GL_TEXTURE_2D of texture unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        // Set uniform to 0 to tell OpenGL the text is bount on tex unit 0
        glUniform1i(baseColorTextureLocation, 0);
      }
      // Metallic and roughness factors
      if (metallicFactorLocation >= 0) {
        glUniform1f(
            metallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(roughnessFactorLocation,
            (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (metallicRoughnessFromTextLocation >= 0) {
        auto textureObject = 0;
        // If there's a texture
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          // If there's a source, get it
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        // Texture unit 1
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(metallicRoughnessFromTextLocation, 1);
      }
      // Emissive factor
      if (emissiveFactorLocation >= 0) {
        glUniform3f(emissiveFactorLocation, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      // Emissive texture
      if (emissiveTextureLocation >= 0) {
        auto textureObject = 0;
        // If there's a texture
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          // If there's an image for the texture
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        // Bind to unity texture 2
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(emissiveTextureLocation, 2);
      }

      // Occlusion
      if (occlusionTextureLocation >= 0) {
        auto textureObject = 0;
        // If there's a texture
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          // If there's an image for the texture
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        // Bind to unity texture 3
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(emissiveTextureLocation, 3);
      }
      if (occlusionStrengthLocation >= 0) {
        glUniform1f(occlusionStrengthLocation,
            (float)material.occlusionTexture.strength);
      }

    } else { // ***** If the material has NO base color texture
             // COLOR
      if (baseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(baseColorTextureLocation, 0);
      }
      if (baseColorFactorLocation >= 0) {
        // set to white
        glUniform4f(baseColorFactorLocation, 1, 1, 1, 1);
      }
      // METALLIC/ROUGHNESS
      if (metallicFactorLocation >= 0) {
        glUniform1f(metallicFactorLocation, 1.f);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(roughnessFactorLocation, 1.f);
      }
      if (metallicRoughnessFromTextLocation >= 0) {
        // /!\ texture unit 1
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(baseColorTextureLocation, 1);
      }
      // EMISSIVE
      if (emissiveFactorLocation >= 0) {
        glUniform3f(baseColorFactorLocation, 0.f, 0.f, 0.f);
      }
      if (emissiveTextureLocation >= 0) {
        // /!\ texture unit 2
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(baseColorTextureLocation, 2);
      }
      // OCCLUSION
      if (occlusionStrengthLocation >= 0) {
        glUniform1f(occlusionStrengthLocation, 0.f);
      }
      if (occlusionTextureLocation >= 0) {
        // /!\ texture unit 3
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(baseColorTextureLocation, 3);
      }
    }
    if (applyOcclusionLocation >= 0) {
      glUniform1i(applyOcclusionLocation, applyOcclusion);
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          const auto &node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix =
              getLocalToWorldMatrix(node, parentMatrix);

          // if the node has a mesh
          if (node.mesh >= 0) {

            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix =
                glm::transpose(glm::inverse(modelViewMatrix));

            // send matrices to shaders
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewProjMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            // Light info

            if (lightDirectionLocation >= 0) {
              // If the light is coming from the camera
              if (lightFromCamera) {
                glUniform3f(lightDirectionLocation, 0.f, 0.f, 1.f);
              } else {
                // Transform to view space and normalize direction
                const auto lightDirectionInVS = glm::normalize(
                    glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.f)));
                glUniform3f(lightDirectionLocation, lightDirectionInVS[0],
                    lightDirectionInVS[1], lightDirectionInVS[2]);
              }
            }

            if (lightIntensityLocation >= 0) {
              // Send intensity to shader
              glUniform3f(lightIntensityLocation, lightIntensity[0],
                  lightIntensity[1], lightIntensity[2]);
            }

            // get mesh and its VAO range
            const auto &mesh = model.meshes[node.mesh];
            // reminder: meshToVertexArrays contains range of VAOs for each
            // mesh
            const auto &vaoRange = meshToVertexArrays[node.mesh];

            // loop over primitives of the mesh
            for (size_t primIdx = 0; primIdx < mesh.primitives.size();
                 ++primIdx) {

              // get vao and bind it
              const auto vao = vertexArrayObjects[vaoRange.begin + primIdx];
              const auto &primitive = mesh.primitives[primIdx];

              // bind material
              bindMaterial(primitive.material);

              glBindVertexArray(vao);

              // check if primitive has indices
              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;
                // reminder: void glDrawElements(GLenum mode, GLsizei count,
                // GLenum type, const GLvoid * indices);
                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {

                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }

          // Recursively draw children
          for (const auto child : node.children) {
            drawNode(child, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // loop over all nodes of the scene
      for (const auto node : model.scenes[model.defaultScene].nodes) {
        drawNode(node, glm::mat4(1));
      }
    }
  };

  // Render image
  if (!m_OutputPath.empty()) {
    const size_t numComponents = 3; // RGB
    std::vector<unsigned char> pixels(
        m_nWindowWidth * m_nWindowHeight * numComponents);
    renderToImage(m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data(),
        [&]() { drawScene(cameraController->getCamera()); });

    // OpenGL doesn't use same convention as png files so we need to flip the
    // image
    flipImageYAxis(
        m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());

    // output the png
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0; // to leave in case we only need to render an image
  }

  // Loop until the user closes the window (RENDER LOOP)
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        // Radio buttons in camera header
        static int cameraType = 0;
        // True only if we change cameras
        const auto changedCamera =
            ImGui::RadioButton("Trackball Camera", &cameraType, 0) ||
            ImGui::RadioButton("First Person Camera", &cameraType, 1);

        if (changedCamera) {

          // std::clog << "changed: " << std::endl;

          const Camera currentCamera = cameraController->getCamera();
          // If tb selected
          if (cameraType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.25f * maxDistance);
          } else {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.25f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
      }

      // Light section

      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        static float theta = 0.f;
        static float phi = 0.f;

        // 2 sliders: teta[0, pi] and phi[0, 2pi]
        // Modify light direction with spherical coordinates
        if (ImGui::SliderFloat("Theta", &theta, 0, 3.14f) ||
            ImGui::SliderFloat("Phi", &phi, 0, 2.f * 3.14f)) {
          const float sinPhi = glm::sin(phi);
          const float cosPhi = glm::cos(phi);
          const float sinTheta = glm::sin(theta);
          const float cosTheta = glm::cos(theta);
          lightDirection =
              glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        // Modify light intensity using a color mutliplied with a scalar
        static float lightColor[3] = {1.f, 1.f, 1.f};
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("Color", lightColor) ||
            ImGui::InputFloat("Intensity", &lightIntensityFactor)) {
          lightIntensity.x = lightColor[0] * lightIntensityFactor;
          lightIntensity.y = lightColor[1] * lightIntensityFactor;
          lightIntensity.z = lightColor[2] * lightIntensityFactor;
        }

        // Lighting from camera
        // If checked, lightDirection is (0, 0, 1) ie -camera.front()
        ImGui::Checkbox("Light from camera", &lightFromCamera);

        // Apply occlusion
        ImGui::Checkbox("Apply occlusion", &applyOcclusion);
      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

//------------------------------------------------------

// load the correct file into the model
bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }

  return true;
}

//------------------------------------------------------

// computes the vector of buffer objects from a model and returns it
std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

  glGenBuffers(GLsizei(model.buffers.size()),
      bufferObjects.data()); // create buffer objects

  // fill each buffer object with the data
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0); // Cleanup the binding point

  return bufferObjects;
}

//------------------------------------------------------

// takes the model and the buffer objects to create an array of VAOs and
// return it, also fills the input vector meshIndexToVaoRange with the range
// of VAOs for each mesh
std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshToVertexArrays)
{

  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

  std::vector<GLuint> vertexArrayObjects; // We don't know the size yet

  // For each mesh of our model we keep its range of VAOs
  meshToVertexArrays.resize(model.meshes.size());

  // Each primitive should have a VAO. But the top level description of
  // geometry is made with "meshes". And each mesh can have multiple
  // "primitives". CCL: only way to know the total number of primitives (= nb
  // VAOs) is to loop over the meshes and add the number of primitives of each
  // one OR use resize() to extend the vector as required at each loop turn.

  // loop over meshes
  for (size_t i = 0; i < model.meshes.size(); ++i) {
    const auto &mesh = model.meshes[i];

    auto &vaoRange = meshToVertexArrays[i];

    vaoRange.begin =
        GLsizei(vertexArrayObjects.size()); // Range for this mesh will be at
                                            // the end of vertexArrayObjects
    vaoRange.count =
        GLsizei(mesh.primitives.size()); // One VAO for each primitive

    // Add enough elements to store our VAOs identifiers
    vertexArrayObjects.resize(
        vertexArrayObjects.size() + mesh.primitives.size());

    glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoRange.begin]);

    // loop over primitives
    for (size_t primitiveIdx = 0; primitiveIdx < mesh.primitives.size();
         ++primitiveIdx) {

      // vao of current primitive
      const auto vao = vertexArrayObjects[vaoRange.begin + primitiveIdx];
      const auto &primitive = mesh.primitives[primitiveIdx];
      glBindVertexArray(vao);

      //  We need to enable and initialize the parameters for each vertex
      //  (position, normal, text)

      // _______________________POSITION attribute___________________________
      {
        // scope: so we can declare const variables with the same name each
        // time
        const auto iterator = primitive.attributes.find("POSITION");

        // if "POSITION" has been found in the map
        if (iterator != end(primitive.attributes)) {

          // (*iterator).first is the key "POSITION", (*iterator).second is
          // the value, ie. the index of the accessor for this attribute

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          // Enable the vertex attrib array corresponding to POSITION
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);

          // Bind the buffer object to GL_ARRAY_BUFFER
          assert(GL_ARRAY_BUFFER == bufferView.target);
          // Theorically we could also use bufferView.target, but it is safer
          // /!\ the next call (glVertexAttribPointer) will use what is
          // currently bound
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          // tinygltf converts strings like "VEC3, "VEC2" to the number of
          // components, stored in accessor.type

          // Compute the total byte offset using the accessor and the
          // buffer view
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

          // Info for glVertexAttribPointer (defines an array of generic
          // vertex attribute data): size -> accessor.type, type ->
          // accessor.componentType stride -> bufferView, normalized =
          // GL_FALSE pointer = byteOffset (cast!)
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }

      // _______________________NORMAL attribute___________________________
      {
        const auto iterator = primitive.attributes.find("NORMAL");

        if (iterator != end(primitive.attributes)) {

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          assert(GL_ARRAY_BUFFER == bufferView.target);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)(accessor.byteOffset + bufferView.byteOffset));
        }
      }

      // ____________________TEXCOORD_0 attribute___________________________
      {
        const auto iterator = primitive.attributes.find("TEXCOORD_0");

        if (iterator != end(primitive.attributes)) {

          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          assert(GL_ARRAY_BUFFER == bufferView.target);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)(accessor.byteOffset + bufferView.byteOffset));
        }
      }

      // Index array if defined
      if (primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
            bufferObjects[bufferIdx]); // Binding the index buffer to
                                       // GL_ELEMENT_ARRAY_BUFFER while the
                                       // VAO is bound is enough to tell
                                       // OpenGL we want to use that index
                                       // buffer for that VAO
      }
    }
  }
  glBindVertexArray(0);

  std::clog << "Number of VAOs: " << vertexArrayObjects.size() << std::endl;

  return vertexArrayObjects;
}

// ----------------------------------------------------------------------

// Computes a vector of texture objects
// Each object is filled with an image and sampling parameters
std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{

  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  // Define default sampler
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  // Generate the texture objects
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());

  // loop over textures
  for (size_t textIdx = 0; textIdx < model.textures.size(); ++textIdx) {

    // Get texture
    const auto &texture = model.textures[textIdx];
    assert(texture.source >= 0);
    // Get image
    const auto &image = model.images[texture.source];

    // Set sampler
    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

    // Bind texture object to target GL_TEXTURE_2D
    glBindTexture(GL_TEXTURE_2D, textureObjects[textIdx]);

    // Fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());

    // Texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);

    // Wrapping modes
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    // Compute mipmaps
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }

  glBindTexture(GL_TEXTURE_2D, 0);
  return textureObjects;
}
// ----------------------------------------------------------------------

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

// https://gltf-viewer-tutorial.gitlab.io/camera/
// lalala