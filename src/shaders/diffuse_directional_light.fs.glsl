#version 330 core

// to give info about the light to the shader
uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;
 
out vec3 fFragColor;

void main() {
	// uLightDirection already normalized
	vec3 VSNormal = normalize(vViewSpaceNormal);
	// simple BRDF to give an approximation of a white diffuse material
	fFragColor = vec3(1./3.14) * uLightIntensity * dot(VSNormal, uLightDirection);

};
 

